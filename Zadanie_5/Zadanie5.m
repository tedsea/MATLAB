%Kp=1;
%Kp=10;
Kp=100;

Gr=Kp;
G=tf([18], [1 10 20]);
figure(1);
step(G, 2);

Go=Gr*G;
%Go =
% 
%        18
%  ---------------
%  s^2 + 10 s + 20

% Transmitancja uk�adu zamkni�tego
Gz=feedback(Go,1);
roots([1 10 20 + Kp*18]);
%Gz =
% 
%        18
%  ---------------
%  s^2 + 10 s + 38
figure(2);
step(Gz);
