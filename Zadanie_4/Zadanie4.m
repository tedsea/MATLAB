Kp=2;
Ti=3;
Td=4;
N=20;
% Test zmiany N
%N=200;
Gr=pidstd(Kp, Ti, Td, N); % Dla nastawy Kp = 2
%Gr =
% 
%             1      1              s      
%  Kp * (1 + ---- * --- + Td * ------------)
%             Ti     s          (Td/N)*s+1 
%
%  with Kp = 2, Ti = 3, Td = 4, N = 20
% 
%Continuous-time PIDF controller in standard form
%Gr1=pidstd(4, Ti, Td, N); % Dla nastawy Kp = 4
%Gr2=pidstd(8, Ti, Td, N); % Dla nastawy Kp = 8
% Dla zmiany Ti
%Gr1=pidstd(2, 6, Td, N); % Dla nastawy Ti = 6
%Gr2=pidstd(2, 10, Td, N); % Dla nastawy Ti = 10
% Dla zmiany Td
Gr1=pidstd(2, 3, 8, N); % Dla nastawy Td = 8
Gr2=pidstd(2, 3, 40, N); % Dla nastawy Td = 12
figure(1);
step(Gr,Gr1,Gr2,10);
grid;
figure(2);
nyquist(Gr);
figure(3);
bode(Gr);
grid;


